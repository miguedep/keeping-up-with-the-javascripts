// all men have the propierty motal = true
function Men() { 
    this.mortal = true;
}

// Socrates is part of a collection of items referred to as Men
const socrates = new Men();

// If socrates is a Men and Men are mortal then socrates is mortal 
if ( socrates instanceof Men && socrates.mortal ){
console.log("Socrates is mortal");
} else {
console.log("Socrates is not mortal");
}  
  
  
  
  
// Extra Credit

// list of possible cake flavors
const flavors = ["chocolate", "vanilla"];

// if cake flavor is not chocolate then cake is vanilla else is chocolate
function cake(flavor) {
if (flavor !== flavors[0]) {
    flavor = flavors[1];
} else {
    flavor = flavors[0];
}
console.log("The cake is " + flavor);
}

// call cake("vanilla")
cake(flavors[1]);