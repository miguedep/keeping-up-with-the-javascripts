const timeAdder = (value1, label1, value2, label2) => {
    let value3, label3;
    // check if values matches with units (1 second not 1 seconds)
    if (value1 > 1 && label1.slice(-1) !== 's' || value1 === 1 && label1.slice(-1) === 's'){ return false; }
    if (value2 > 1 && label2.slice(-1) !== 's' || value2 === 1 && label2.slice(-1) === 's'){ return false; }
  
  // convert to seconds
  const convertToSeconds = (value, label) => {
    if (Number.isInteger(value) && value > 0){
      switch(label){
        case "seconds": case "second":
          value = value * 1;
          break;
        case "minutes": case "minute":
          value = value * 60;
          break;
        case "hours": case "hour":
          value = value * 60 * 60;
          break;
        case "days": case "day":
          value = value * 60 * 60 * 24;
          break;
        default:
          return false;
      }
      return value;
    } else {
      return false;
    }      
  };

  const value1Seconds = convertToSeconds(value1, label1);
  const value2Seconds = convertToSeconds(value2, label2);

  // check if the conversion to seconds is not false 
  // return the value in the biggest UoM. Match singular and plural.    NO 36000 seconds   ====>   YES 1 hour
  if (value1Seconds && value2Seconds){
      const totalSeconds = value1Seconds + value2Seconds;
      if(totalSeconds % (60 * 60 * 24) === 0){
          value3 = totalSeconds / (60 * 60 * 24);
          if (value3 > 1) { label3 = "days" } else { label3 = "day" }
      } else if (totalSeconds % (60 * 60) === 0){
        value3 = totalSeconds / (60 * 60);
        if (value3 > 1) { label3 = "hours" } else { label3 = "hour" }
      } else if (totalSeconds % 60 === 0){
        value3 = totalSeconds / 60;
        if (value3 > 1) { label3 = "minutes" } else { label3 = "minute" }
      } else {
          value3 = totalSeconds;
          if (value3 > 1) { label3 = "seconds" } else { label3 = "second" }
      }
      return [value3, label3];
  } else {
      return false;
  }
  
};

console.log(timeAdder(6, "hours", 3600, "seconds"));
