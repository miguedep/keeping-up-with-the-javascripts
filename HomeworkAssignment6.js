// for loop to print 1 to 100
for (var i = 1; i <= 100; i++){
    // check is the actual number i is prime
    let isPrime = true;
    for(var j = 2; j < i; j++){
      // if i is divisible by j then is it not prime
       if(i % j === 0){
         isPrime = false;
       }   
    }
    
    if(isPrime){ // i is Prime
    console.log("Prime");
} else if(i % 15 === 0){ // i is div by 3 and 5
    console.log("FizzBuzz");
} else if (i % 3 === 0){ // i is div by 3
    console.log("Fizz");
} else if (i % 5 === 0){ // i is div by 5
    console.log("Buzz");
} else { // if no condition is meet then print i
    console.log(i);
}
}