const squareWrapper = document.getElementById("rectangleWrapper");
let isPlayer1 = true;
squareWrapper.addEventListener("click", click);
const win = [[r1,r2,r3], [r4,r5,r6]];


function click() {
    const e = event.target;
    const id = event.target.id;
    if (e.nodeName === "P"){
        alert("Sorry, you cant click there!");
    } else if (e.classList[0] === 'rectangle'){
        addMark(id);
    }
};

function addMark(id){
    let player = isPlayer1 ? "X" : "O";
  
    const para = document.createElement("P");
    const t = document.createTextNode(player);
    para.appendChild(t);
    const select = document.getElementById(id);
    select.appendChild(para);
    const paraf = select.querySelector("P");
    paraf.classList.add(player);

    win_tie();

    isPlayer1 = !isPlayer1;
};

function win_tie(){
    console.log(document.querySelectorAll('rectangle'));
};