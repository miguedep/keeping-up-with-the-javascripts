/* 
    This javaScript file contains a few variables that describe a song.
    The object of this file is practice with different types of variables.
*/

// Song's title (str)
var songTitle = "Creep";
// Does it has any Artwork (bool)
var hasArtwork = false;
// Which year it was release (number)
var releaseYear = 1992;
// Artist (str)
var artist = "Radiohead"
// Group components (object), what they perform (array)
var components = {
    "Thom Yorke": ["vocals", "guitar", "piano", "keyboards"],
    "Jonny Greenwood": ["lead guitar", "keyboards", "other instruments"],
    "Colin Greenwood": ["bass"],
    "Ed O'Brien": ["guitar", "backing vocals"],
    "Philip Selway": ["drums", "percussion"],
    };
// Song's genre (str)
var genre = "Rock";
// Length in seconds (number)
var durationInSeconds = 236;

// Print to the console the variable values.
console.log("Title: " + songTitle);
console.log("Artwork? " + hasArtwork);
console.log("Year released: " + releaseYear);
console.log("Artist: " + artist);
console.log("Components: ");
for (key in components){
    console.log("   " + key + ": " + components[key].join(", "));
}
console.log("Genre: " + genre);
console.log("Length: " + durationInSeconds + " seconds");





