/* 
    This javaScript file contains an explanation about the difference between var, let and constant.
    The object of this file is to explain and practise with different types of variables.
*/

/*
    var declares a variable with global scope (outside functions) or local scope (inside a function). Hoisting is allowed.
    let declares a variable with a global scope or block scope (inside a block or a function). Hoisting is not allowed. From ES6.
    const same as let but it is not reassignable but mutable. 
    Hoisting: variable declarations and functions are put in memory during the compiling phase. It is like moving variable declarations and functions to the top of the code.
*/

// Block and function scope

var aGlobal = 1;
if (aGlobal){ // block scope
    var aGlobal = 2; 
    console.log(aGlobal); // aGlobal === 2
}
function a(){ // function scope
    var aGlobal = 3; 
    console.log(aGlobal); // aGlobal === 3
} 
console.log(aGlobal); // aGlobal === 2
a();


let bGlobal = 1;
if (bGlobal){ // block scope
    let bGlobal = 2; 
    console.log(bGlobal); // aGlobal === 2
}
function b(){ // function scope
    let bGlobal = 3; 
    console.log(bGlobal); // aGlobal === 3
} 
console.log(bGlobal); // aGlobal === 1
b();


// const is not reassignable but mutable
const c = [1,2,3];
c.push(4); // Mutable - we can add elements to c
console.log(c); // Array(4) [1, 2, 3, 4]
// c = [1,3,4,4] ERROR - not reassignable


// Hoisting - only var

i = 5;
console.log(i); // 5
var i;

console.log(j); // undefined
j = 5;
var j;

k(); // hello. We put in memory the whole function.
function k(){
    console.log('hello');
}

g(); // ERROR  g is not a function. We only put in memory the variable declaration.
var g = function j(){
    console.log('hello');
}

